from django.contrib import admin
from todo.models import Todo

"""
 아래 데코레이터는 admin.site.register(Todo) 와 같은 역확을 한다.
"""
@admin.register(Todo)
class TodoAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'todo')