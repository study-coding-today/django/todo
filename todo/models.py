from django.db import models


class Todo(models.Model):
    name = models.CharField('NAME', max_length=6, blank=True)
    todo = models.CharField('TODO', max_length=100)

    def __str__(self):
        return self.todo

    # 리스트 생성시 name 을 입력하지 않을때 홍길동을 기본값으로 넣어줌
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.name:
            self.name = '홍길동'
        super().save()

