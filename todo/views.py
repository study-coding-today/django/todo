from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, ListView, DeleteView
from django.views.generic.list import MultipleObjectMixin

from .models import Todo


class TodoVueSee(TemplateView):
    template_name = 'todo/todo_vue_see.html'


# 아래 CreateView 는 form 으로 model 과 연동하여 저장하므로 model를 추가 사용한다.
class TodoCV(CreateView):
    model = Todo
    fields = '__all__'         # model 에 모든 필드 사용
    template_name = 'todo/todo_form.html'
    # success_rul 로 redirect 는 reverse_lazy 사용한다. 모듈이 완성되지 않았기 때문.
    success_url = reverse_lazy('todo:list')
    # 함수로 반전시에는 reverse() 를 사용한다.


class TodoLV(ListView):
    model = Todo        # 리스트를 가져오려면 db table 에서 가져오므로 model 사용.
    template_name = 'todo/todo_list.html'


class TodoDelV(DeleteView):
    model = Todo        # list 삭제도 table 에서 삭제하므로 model 사용.
    template_name = 'todo/todo_confirm_delete.html'
    # 삭제후 리로드 하기 위해서 reverse_lazy 사용.
    success_url = reverse_lazy('todo:list')


# 페이지 이동없이 mixin 을 이용하여 추가하기
# object_list 를 Template 로 보내기 위해서( note.txt 참조)
class TodoMulObMixCV(MultipleObjectMixin, CreateView):
    model = Todo
    fields = '__all__'
    template_name = 'todo/todo_form_list.html'
    success_url = reverse_lazy('todo:mixin')

    # self.object_list 만들기 위해 선언하고 Todo model 에서 가져오도록 만든다.
    # get, post 에서 만들어 사용한다.
    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return super().post(request, *args, **kwargs)


# 페이지 이동없이 mixin 을 이용하여 삭제하기
class TodoDelV2(DeleteView):
    model = Todo        # list 삭제도 table 에서 삭제하므로 model 사용.
    template_name = 'todo/todo_confirm_delete.html'
    # 삭제후 리로드 하기 위해서 reverse_lazy 사용.
    success_url = reverse_lazy('todo:mixin')

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)

