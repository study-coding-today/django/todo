from django.views.generic import TemplateView


# home.html 은 특정 앱에 포함된 것이 아니니 config 에서 사용한다.
class HomeView(TemplateView):
    template_name = 'home.html'
